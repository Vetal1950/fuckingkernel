import {sender} from "./baseClasses/sendClass";
import {util} from "./baseClasses/utilityClass";
import {service} from "./baseClasses/scheduler";

/**
 * Модуль для отправки отбойников о превышении сроков исполнения заявки
 * */
class pluginClass extends service {
    constructor() {
        super(name);
        this.lastDay = -1;
    }

    async __getOldTasks(){
        let nowDate = new Date();
        nowDate.setHours(0);
        let filter = {
            comparisons: {
                deadline: {
                    left: {
                        type: "field",
                        value: "deadline"
                    },
                    right: {
                        type: "value",
                        value: util.dateToStr(nowDate)
                    },
                    sign: "less"
                }
            },
            tree: {and: ["deadline"]}
        };
        return await sender.send({
            object: `helpDesk.stages_inWorkHD`,
            method: "get",
            parameters: {
                filter: filter,
                fields: [
                    "categoryID.initiatorID.description",
                    "categoryID.initiatorID.email",
                    "queryID.description",
                    "queryID.problemUserID.description",
                    "queryID.initiatorID.description",
                    "executorID.description",
                    "executorID.email"
                ]
            }
        });
    }

    __formTableForMentor(taskArray){
        let titleRow = [util.HTMLRowFromArray([
            `<b>Заявка</b>`, `<b>Время выполнения</b>`, `<b>Инициатор</b>`, `<b>Исполнитель</b>`, `<b>Почта исполнителя</b>`, `<b>Комментарий</b>`
        ])];
        return `<table border="2">${titleRow}${taskArray.map(util.HTMLRowFromArray).join("")}</table>`;
    }

    __getMentorsEmails(tasks){
        let mentors = {};
        for(let tID in tasks){
            let task = tasks[tID];
            let mentorEmail = util.getInnerFieldFromStructure(task, "categoryID.initiatorID.email")[0],
                mentorName = util.getInnerFieldFromStructure(task, "categoryID.initiatorID.description")[0];
            if(!mentorEmail){
                continue;
            }
            if(!mentors[mentorEmail]){
                mentors[mentorEmail] = {
                    name: mentorName,
                    tasks: []
                }
            }
            mentors[mentorEmail].tasks.push([
                util.getInnerFieldFromStructure(task, "queryID.description")[0],
                util.dateToStr(task.fields.date),
                util.getInnerFieldFromStructure(task, "queryID.problemUserID.description")[0]
                || util.getInnerFieldFromStructure(task, "queryID.initiatorID.description")[0],
                util.getInnerFieldFromStructure(task, "executorID.description")[0],
                util.getInnerFieldFromStructure(task, "executorID.email")[0],
                task.fields.commentary || ""
            ]);
        }

        let emails = [];
        for(let email in mentors){
            emails.push({
                to: email,
                description: "Уведомление о превышении срока выполнения заявок",
                body: `Добрый день! <br>
                <br>
                В курируемых вами заявках превышен срок выполнения:<br>
                ${this.__formTableForMentor(mentors[email].tasks)}
                `
            });
        }
        return emails;
    }

    __formTableForExecutor(taskArray){
        let titleRow = [util.HTMLRowFromArray([
            `<b>Заявка</b>`, `<b>Время выполнения</b>`, `<b>Инициатор</b>`, `<b>Комментарий</b>`
        ])];
        return `<table border="2">${titleRow}${taskArray.map(util.HTMLRowFromArray).join("")}</table>`;
    }
    __getExecutorsEmails(tasks){
        let executors = {};
        for(let tID in tasks){
            let task = tasks[tID];
            let executorEmail = util.getInnerFieldFromStructure(task, "categoryID.initiatorID.email")[0],
                executorName = util.getInnerFieldFromStructure(task, "categoryID.initiatorID.description")[0];
            if(!executorEmail){
                continue;
            }
            if(!executors[executorEmail]){
                executors[executorEmail] = {
                    name: executorName,
                    tasks: []
                }
            }
            executors[executorEmail].tasks.push([
                util.getInnerFieldFromStructure(task, "queryID.description")[0],
                util.dateToStr(task.fields.date),
                util.getInnerFieldFromStructure(task, "queryID.problemUserID.description")[0]
                || util.getInnerFieldFromStructure(task, "queryID.initiatorID.description")[0],
                task.fields.commentary || ""
            ]);
        }

        let emails = [];
        for(let email in executors){
            emails.push({
                to: email,
                description: "Уведомление о превышении срока выполнения заявок",
                body: `Добрый день! <br>
                <br>
                У некоторых выполняемых вами заявок превышен период выполнения:<br>
                ${this.__formTableForExecutor(executors[email].tasks)}
                `
            });
        }
        return emails;
    }

    __sendEmails(emails){
        if(emails.length === 0){
            return;
        }
        return sender.send({
            object: `helpDesk.emailSender`,
            method: "insert",
            parameters: {
                values: emails
            }
        });
    }

    async run(parameters, token, deep = 0) {
        let nowDate = new Date();
        if(this.lastDay !== nowDate.getDate()) {
            let oldTasksRecords = await this.__getOldTasks();

            let mentorsTasks = this.__getMentorsEmails(oldTasksRecords);
            let executorTasks = this.__getExecutorsEmails(oldTasksRecords);

            await this.__sendEmails(mentorsTasks.concat(executorTasks));
            this.lastDay = nowDate.getDate();
        }
    }
}

let name = "deadlineReport_helpDesk";
let plugin = new pluginClass();

export {
    plugin as plugin,
    name as name
}
