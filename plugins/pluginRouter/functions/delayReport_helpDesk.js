import {sender} from "./baseClasses/sendClass";
import {util} from "./baseClasses/utilityClass";
import {service} from "./baseClasses/scheduler";

/**
 * Модуль для отправки отбойников о необходимости назнавить исполнителя у задачи
 * */
class pluginClass extends service {
    constructor() {
        super(name);
    }

    __getCurrentTasksHD() {
        let nowDate = new Date();
        nowDate.setMinutes(nowDate.getMinutes() - this.__config.schedulerTimeout);
        let filter = {
            comparisons: {
                date: {
                    left: {
                        type: "field",
                        value: "date"
                    },
                    right: {
                        type: "value",
                        value: util.dateToStr(nowDate)
                    },
                    sign: "less"
                }
            },
            tree: {and: ["date"]}
        };

        return sender.send({
            object: "helpDesk.stages_setExecutorHD",
            method: "get",
            parameters: {
                filter: filter,
                fields: [
                    "categoryID.initiatorID.description",
                    "categoryID.initiatorID.email",
                    "queryID.description",
                    "queryID.problemUserID.description",
                    "queryID.initiatorID.description"
                ]
            }
        });
    }

    async __getSortTasks() {
        let tasks = await this.__getCurrentTasksHD();
        let sortTasks = {};
        for (let tID in tasks) {
            let task = tasks[tID];
            let mentorEmail = util.getInnerFieldFromStructure(task, "categoryID.initiatorID.email")[0],
                mentorName = util.getInnerFieldFromStructure(task, "categoryID.initiatorID.description")[0];

            if (!sortTasks[mentorEmail]) {
                sortTasks[mentorEmail] = {
                    name: mentorName,
                    tasks: []
                }
            }

            sortTasks[mentorEmail].tasks.push([
                util.getInnerFieldFromStructure(task, "queryID.description")[0],
                util.dateToStr(task.fields.date),
                util.getInnerFieldFromStructure(task, "queryID.problemUserID.description")[0]
                || util.getInnerFieldFromStructure(task, "queryID.initiatorID.description")[0],
                task.fields.commentary || ""
            ]);
        }
        return sortTasks;
    }

    __formTableForTasks(taskArray){
        let titleRow = [util.HTMLRowFromArray([
            `<b>Заявка</b>`, `<b>Время создания</b>`, `<b>Инициатор</b>`, `<b>Комментарий</b>`
        ])];
        return `<table border="2">${titleRow}${taskArray.map(util.HTMLRowFromArray).join("")}</table>`;
    }

    async __sendEmailsToMentors(mentorsTasks){
        let letters = [];
        for(let mentorEmail in mentorsTasks) {
            letters.push({
                description: "Уведомление о необходимости определения исполнителя",
                to: mentorEmail,
                body: `Вам необходимо принять решение для определения исполнителя для следующих задач:<br>
                ${this.__formTableForTasks(mentorsTasks[mentorEmail].tasks)}<br><br>
                <a href="http://gag-test.groupstp.ru/main.html#objView=helpDesk&object=queryExecutionHD">Ссылка на систему</a>`
            });
        }

        await sender.send({
            object: "helpDesk.emailSender",
            method: "insert",
            parameters: {
                values: letters
            }
        });

        console.log("sended");
    }

    async run(parameters, token, deep = 0) {
        let tasks = await this.__getSortTasks();
        await this.__sendEmailsToMentors(tasks);
    }
}

let name = "delayReport_helpDesk";
let plugin = new pluginClass();

export {
    plugin as plugin,
    name as name
}
