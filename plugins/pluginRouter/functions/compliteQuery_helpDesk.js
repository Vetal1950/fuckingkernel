import {sender} from "./baseClasses/sendClass";
import {util} from "./baseClasses/utilityClass";
import {service} from "./baseClasses/service";

/**
 * Модуль для отправки отбойников о закрытии заявок
 * */
class pluginClass extends service {
    constructor() {
        super(name);
    }

    __getSetExecutorHistory(qids){
        return sender.send({
            object: `helpDesk.queryExecutionHD`,
            method: "get",
            parameters: {
                filter: {
                    comparisons:{
                        qids:{
                            left: {
                                type: "field",
                                value: "qid"
                            },
                            right: {
                                type: "value",
                                value: qids
                            },
                            sign: "in"
                        },
                        stage: {
                            left: {
                                type: "field",
                                value: "stage"
                            },
                            right: {
                                type: "value",
                                value: "stages_inWorkHD"
                            },
                            sign: "equal"
                        }
                    },
                    tree: {and: ["qids", "stage"]}
                },
                parameters:{
                    unique: ["qid"],
                    orderBy: [{
                        field: 'date',
                        sort: 'ASC'
                    }]
                }
            }
        });
    }

    async run(parameters, token, deep = 0) {

        let tasks = this.__getAcceptenceTasks();
        let qids = [];
        for (let taskID in tasks) {
            qids.push(Object.keys(tasks[taskID].fields.queryID)[0]);
        }
        if (qids.length === 0) {
            return;
        }
    }
}

let name = "compliteQuery_helpDesk";
let plugin = new pluginClass();

export {
    plugin as plugin,
    name as name
}
