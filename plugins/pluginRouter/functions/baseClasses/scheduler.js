let sch = require('node-schedule');
import {service} from "./service"

class scheduler extends service{
    constructor(...args){
        super(...args);
        let self = this;
        let runFunction = this.run;
        console.log(`start sch init: ${this.__config.schedulerTimeout}`);
        this.scheduler = sch.scheduleJob(`*/${this.__config.schedulerTimeout} * * * *`, function(){
            if(self.__workTime()) {
                runFunction.call(self);
            }
        });
    }

    __workTime(){
        let nowTime = new Date();
        return (nowTime.getDay() > 0) && (nowTime.getDay() < 6) &&
            (nowTime.getHours() >= 8) && (nowTime.getHours() <= 19);
    }
}

export {scheduler as service}